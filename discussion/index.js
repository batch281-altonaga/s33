// Introduction to Postman and REST API
/**
 * ? What is an API?
 * * Application Programming Interface (API)
 * * Part of a server responsible for handling requests
 * ! Sets the rules of interaction between front and back ends of an app
 */

/* What is REST?
 * Representational State Transfer
 * Architectural style
 * Set of principles for designing networked applications and building web services
 */

/* REST API Methods
 * GET - collects information
 * POST - sends information
 * PUT - updates information
 * DELETE - deletes information
 */

// A path to the resource to be operated on
// https://www.example.com/api/users

// [Section] Getting all posts
// What is fetch?
// A built-in web API that allows us to make HTTP requests to servers from our JS code

// A "promise" is an object that represents the eventual result of an asynchronous function
// and its resulting value

// Create a simple fetch request
const placeholder = fetch("https://jsonplaceholder.typicode.com/posts");

console.log(placeholder);

// Check status of the request
placeholder.then((response) => console.log(response.status));

placeholder
  .then((response) => response.json())
  .then((data) => console.log(data));

// Demonstrate using the "async" and "await" keywords
/**
 * ? What is async/await?
 * * Async/await is a new way to write asynchronous code
 * * Async/await is built on top of promises
 * * It is used in functions to indicate that they are asynchronous
 */

async function fetchData() {
  let result = await fetch("https://jsonplaceholder.typicode.com/posts");
  console.log(result);
  console.log(typeof result);
  // We cannot access the content of the "Response" by directly accessing its body's property
  console.log(result.body);

  // Convert data to JSON
  let json = await result.json();
  console.log(json);
}

fetchData();
// Process a GET Request using Postman
/* 
    postman:
    url: 
*/

// Getting a specific post
fetch("https://jsonplaceholder.typicode.com/posts/1", [
  {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      title: "New post",
      body: "Hello World",
      userID: 1,
    }),
  },
])
  .then(response, console.log(response.status))
  .then((data) => console.log(data));

/*
 * PUT is used to update the whole object
 * PATCH is used to update a part of the object
 */

// Updating a post using PATCH
fetch(
  "https://jsonplaceholder.typicode.com/posts/1",
  {
    method: "PATCH",
    headers: {
      "Content-type": "applicaton/json",
    },
  },
  {
    body: JSON.stringify({
      title: "Corrected post",
    }),
  }
)
  .then((response) => console.log(response.json))
  .then((data) => console.log(data));

// Deleting a specific post following the REST API
fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "DELETE",
});

/**
 * ? Filtering posts
 * * The data can be filtered by sending the userID along with the URL
 * * Information sent via the url can be done by adding the question mark symbol
 */
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then((response) => console.log(response.json))
.then((data) => console.log(data));